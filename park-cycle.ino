
//librerias


#include <MFRC522.h>
#include <EEPROM.h>  //COntrola E/S EEPROM
#include <Wire.h>// libreria para acceder a comunicacion i2c
#include <LiquidCrystal_I2C.h>// libreria para manejar lcd con i2c
#include <Keypad.h>
#include <SPI.h>

char sensorVal;


LiquidCrystal_I2C lcd(0x3F,16,2); //conf objeto  lcd_i2c ("direccion i2c", "columnas", "filas")
 
const byte rowsCount = 4;
const byte columsCount = 3;

 byte rowPins[rowsCount] = {8,7, 6, 5 };
 byte columnPins[columsCount] = { 4, 3, 2 };
 
char keys[rowsCount][columsCount] = {
              {'1','2','3'},
              {'4','5','6'},
              {'7','8','9'},
              {'*','0','#'}
};
 

 
Keypad keypad = Keypad(makeKeymap(keys), rowPins, columnPins, rowsCount, columsCount);

#define RST_PIN  9    //Pin 9 para el reset del RC522
#define SS_PIN  10   //Pin 10 para el SS (SDA) del RC522
MFRC522 mfrc522(SS_PIN, RST_PIN); //Creamos el objeto para el RC522


boolean tecla = false;
boolean card = false;
char key;
byte readCard[4];
byte readCard2[4];
char password[4];
char passUser[4]; 
 

void setup() {
  // CONFIGURACION INICIAL

  Serial.begin(9600);
  
  Wire.begin();
   // Inicializar el LCD
  lcd.init();
  //Encender la luz de fondo.
  lcd.backlight();
  SPI.begin();        //Iniciamos el Bus SPI
  mfrc522.PCD_Init(); // Iniciamos  el MFRC522
  

  //pinMode(14, INPUT_PULLUP);
  

pinMode(15, INPUT);
pinMode(16, OUTPUT);


   
}

void loop() {
lcd.setCursor(3, 0);
lcd.print("PARK-CYCLE");
card =false;

sensorVal = digitalRead(15);



while(sensorVal == HIGH){
   ingresar();
  }


  char key_menu = keypad.getKey();

  if (key_menu)
  {
     if (key_menu == '*')
      { 
        menu();
        lcd.clear();
        key_menu == NO_KEY;
        }
    
  }

}






void ingresar() {

while(sensorVal == HIGH){
 char i; 
  lcd.setCursor(1, 0);
  lcd.print("INPUT PASSWORD");
  //lcd.setCursor(5, 1);
  
char key = keypad.getKey();

  if (key)
  {

    
    password[i++]=key;
    lcd.setCursor(5+i, 1);
    lcd.print(key);
    
    
  }
  if(i==4)
  {
  Serial.print(password);
  lcd.clear();
  lcd.print("success");
  digitalWrite(16, HIGH);
  delay(2000);
  i=0;
  lcd.clear();
  salida();
  }

}

}


void salida()
{
 boolean correct;
  char j;
  while(sensorVal == HIGH)
  {
  

  lcd.setCursor(0, 0);
  lcd.print("WAIT.. PASSWORD");


     if ( mfrc522.PICC_IsNewCardPresent()) 
        {  
      //Seleccionamos una tarjeta
            if ( mfrc522.PICC_ReadCardSerial()) 
            {
                  // Enviamos serialemente su UID
                  Serial.print("Card UID:");
                  for (byte i = 0; i < mfrc522.uid.size; i++) {
                          readCard2[i] = mfrc522.uid.uidByte[i];
                          Serial.print(readCard2[i], HEX);
                  } 

                  for (byte j = 0; j < mfrc522.uid.size; j++) {
                         if(readCard2[j]==EEPROM.read(2 + j)){
                            correct = true;
                          }
                         else {
                           correct = false;
                          }
                  }

                   if(correct)
                      { lcd.clear();
                        lcd.print("Card Correct");
                        digitalWrite(16,LOW);
                        delay(3000);
                          for (byte k=0; k<4; k++)
                        {
                         password[k] = passUser[k] = ' ';
                              
                            }
                        lcd.clear();
                        loop();
                       }else
                       lcd.clear();
                       lcd.print("Card inCorrect");
                       delay(3000);
                        }
                                        
              
                  
                  // Terminamos la lectura de la tarjeta  actual
                  mfrc522.PICC_HaltA();  
                  lcd.clear();
                       
                       
            }      
  
  
  char keytwo = keypad.getKey();

  if (keytwo)
  {

    
    passUser[j++]=keytwo;
    lcd.setCursor(5+j, 1);
    lcd.print(keytwo);
    
    
  }

    if(j==4)
  {
  Serial.print(passUser);
  lcd.clear();
  lcd.print("comparando..");
  delay(2000);
  j=0;
  lcd.clear();
  comparacion();
  }
  
  }
  }

void comparacion(){ 
boolean correct;
char i,j;


  for (i=0; i<4; i++)
 {
    if(password[i]==passUser[i]){
        correct= true;
      }
      else
      correct = false;
 }

if(correct)
{
  lcd.print("Pass Correct");
  digitalWrite(16,LOW);
  delay(3000);
    for (j=0; j<4; j++)
 {
   password[j] = passUser[j] = ' ';
        
      }
  lcd.clear();
  loop();
 }else
 {
 lcd.print("Pass inCorrect");
 delay(3000);
 }
  
  }


void menu() {
  lcd.clear();
  while(!card) {
    // Escribimos el Mensaje en el LCD.


  
  char key_menu = keypad.getKey();

  if (key_menu)
  {
     if (key_menu == '#')
      { 
        
        lcd.clear();
        key_menu == NO_KEY;
        loop();
        card=true;
        
        }
    
  }
  
  lcd.setCursor(0,0);  
  lcd.print("Agregar key");
  lcd.setCursor(0,1);
  lcd.print("Acerque la tarjeta al lector");
   if ( mfrc522.PICC_IsNewCardPresent()) 
        {  
      //Seleccionamos una tarjeta
            if ( mfrc522.PICC_ReadCardSerial()) 
            {
                  // Enviamos serialemente su UID
                  Serial.print("Card UID:");
                  for (byte i = 0; i < mfrc522.uid.size; i++) {
                          readCard[i] = mfrc522.uid.uidByte[i];
                          EEPROM.write( 2 + i, readCard[i] );
                          Serial.print(readCard[i], HEX);
                             
                  } 
                  Serial.println();
                  // Terminamos la lectura de la tarjeta  actual
                  mfrc522.PICC_HaltA();  
                  lcd.clear();
                  lcd.setCursor(0,1);
                  lcd.print("add card success");
                  delay(1000);
                  lcd.clear();
                  card =true;
                                   
                       
            }      
  }

  }
  
  }

